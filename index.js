///////////////////////////////////////////////////////
///                  VARIABLES                   /////
/////////////////////////////////////////////////////

// images par seconde
let fps = 30;

// variables globales
let map = createArray(40, 30);

// variables pour Dijkstra
let distance = createArray(40, 30); 
let Q = createArray(40, 30);
let predecesseur = createArray(40, 30);
let posDepart, posFin;
let cheminTrouve;

// variable du canvas
let canvas = document.getElementById('moncanvas');
let ctx = canvas.getContext('2d');
let count = document.getElementById('count');


///////////////////////////////////////////////////////
///          Fonctions touches clavier           /////
/////////////////////////////////////////////////////

// capture des touches clavier
document.addEventListener('keydown', keyDownListener);
document.addEventListener('keyup', keyUpListener);

let keyPresses = [];

function keyDownListener(event) {

	// événement touche appuyée
	keyPresses[event.key] = true;
}

function keyUpListener(event) {

	// événement touche relâchée
	keyPresses[event.key] = false;
}


///////////////////////////////////////////////////////
///                    OUTIL                     /////
/////////////////////////////////////////////////////

function createArray(length) {    // 

	// fonction de création d'un tableau à n dimensions
	let arr = new Array(length || 0),
		i = length;

	if (arguments.length > 1) {

		let args = Array.prototype.slice.call(arguments, 1);

		while (i--) {
			arr[length - 1 - i] = createArray.apply(this, args);
			// console.log(arr);
		}
	}

	return arr;
}

function GetRandom(min, max) {

	// retroune un nombre au hazard en min et max inclus
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function init() {

	// fonction d'initialisation quelque soit l'algorithme
	let i, x, y, j, k;

	for (i = 0; i < 40; i++) {
		for (j = 0; j < 30; j++) {

			// remise à zéro de la map
			map[i][j] = 0;

			// sur les bords on génère un mur dont la position x/y a pour valeur: 9 (obstact infranchissable)
			if (i == 0 || j == 0 || i == 39 || j == 29) {
				map[i][j] = 9;
			}
		}
	}

	// position aléatoire du début et de la fin (début à gauche et fin à droite)
	posDepart = GetRandom(1, 28);
	posFin = GetRandom(1, 28);
	map[1][posDepart] = 1;
	map[38][posFin] = 2;

	// on génère des obstacles
	for (i = 1; i <= 15; i++) {

		x = GetRandom(5, 35); y = GetRandom(5, 25); taille = GetRandom(1, 2);

		for (j = x - taille; j <= x + taille; j++) {
			for (k = y - taille; k <= y + taille; k++) {

				// test les positions avant de créer l'obstacle infranchissable pour toujours laisser un accées départ / arriver
				// evite de créer un obstacle coller aux murs
				if (j != 1 && j != 38 && j >= 0 && k >= 0 && j <= 39 && k <= 29) {
					map[j][k] = 9;
				}
			}
		}
	}

	// initialisation de l'algorithme de Dijkstra
	Dijkstra_init();
}

///////////////////////////////////////////////////////
///   Fonctions pour l'algorithme de Dijkstra    /////
/////////////////////////////////////////////////////

// initialise les variables
function Dijkstra_init() {

	let i, j;

	cheminTrouve = false;

	// boucle sur chaque case
	for (i = 0; i < 40; i++) {
		for (j = 0; j < 30; j++) {

			distance[i][j] = 99999; 
			Q[i][j] = true;
			console.log()

			// test la valeur des positions et les réinitialises
			if (map[i][j] == 8) { // 8 = chemin du retour (jaune)
				map[i][j] = 0; // 0 = chemin accessible
			}
		}
	}
	distance[1][posDepart] = 0;
	predecesseur = createArray(40, 30);
}

// Cherche constament la direction ayant le moin de déplacement
function Dijkstra_trouve_min() {

	let mini = 99999;
	let sommetx, sommety;

	sommetx = -1;
	sommety = -1;

	// boucle sur chaque case 
	for (i = 0; i < 40; i++) {
		for (j = 0; j < 30; j++) {

			// filtre les cases ayant une valeur < 2 (chemin)
			if (Q[i][j] && map[i][j] <= 2) {

				// filtre les distance et actualise "mini" avec la distance la plus courte
				if (distance[i][j] < mini) {

					mini = distance[i][j];
					// console.log(mini);
					count.innerHTML="Nbr de déplacement minimum: "+mini;
					sommetx = i; 
					sommety = j;
				}
			}
		}
	}
	return { x: sommetx, y: sommety }
}

// Attribue un poid a chaque case par rapport au valeur attribuer 
function Dijkstra_poids(x1, y1, x2, y2) {

	// retourne un "poids" entre deux points
	// remarque : dans un jeu, on pourrait mettre un poids plus important s'il y a une colline à franchir par exemple.
	if (map[x2][y2] > 2) {

		// si ce n'est pas un espace vide, c'est impossible de passer
		return 99999;

	} else {
		if (x1 == x2 || y1 == y2) {
			// si c'est une case adjacente, on met un poids de 1
			return 1;

		} else {
			// si c'est une diagonale, on met un poids de 1.5 pour favoriser le chemin le plus droit, 3.5 pour éviter les chemins en diagonale
			return 10;
		}
	}
}

function Dijkstra_maj_distances(x1, y1, x2, y2) {
	
	// remplacer dans le if suivant le +1 par le poids entre deux points si nécessaire
	if (distance[x2][y2] > distance[x1][y1] + Dijkstra_poids(x1, y1, x2, y2)) {
		
		distance[x2][y2] = distance[x1][y1] + Dijkstra_poids(x1, y1, x2, y2);
		// console.log(distance[x2][y2]);
		predecesseur[x2][y2] = { x: x1, y: y1 };
	}
}

// Retourne "tout_parcouru = false" tant que les cases n'ont pas étaient totalement testés
function Dijkstra_tout_parcouru() {

	let i, j;
	let tout_parcouru = true;

	// boucle sur chaque case 
	for (i = 39; i >= 0; i--) {
		for (j = 29; j >= 0; j--) {

			if (Q[i][j]) { // valeur affecter dans "Dijkstra_init()" (ligne 125)
				tout_parcouru = false; i = -1; j = -1; 
				// console.log(tout_parcouru);
			}
		}
	}
	
	// console.log(tout_parcouru);
	return tout_parcouru;
}

//   ----------------------------------------
function Dijkstra() {

	let paire;
	let curX, curY, tx, ty;

	// tant que la map n'as pas était totalement découvert et qu'il y a aucun chemin trouver:
	if (!Dijkstra_tout_parcouru() && !cheminTrouve) { 

		paire = Dijkstra_trouve_min(); // paire = x: sommetx, y: sommety (distance minimum)
		// console.log(paire);

		if (paire.x != -1) {

			Q[paire.x][paire.y] = false;
			// console.log(Q)

			for (i = -1; i <= 1; i++) {
				for (j = -1; j <= 1; j++) {

					if (!(i == 0 && j == 0)) {

						Dijkstra_maj_distances(paire.x, paire.y, paire.x + i, paire.y + j);
					}
				}
			}
		}
	}

	// on établit le chemin si la fin a été trouvée
	curX = 38; 
	curY = posFin;

	// si la derniere position correspond à la position de l"Arrivé"
	if (predecesseur[38][posFin]) {
		cheminTrouve = true; // chemin trouver

		// tant que la position de l"Arrivé" est différent du "Départ" 
		while (!(curX == 1 && curY == posDepart)) {

			if (!(curX == 38 && curY == posFin) && !(curX == 1 && curY == posDepart)) {
				map[curX][curY] = 8;
			}

			tx = predecesseur[curX][curY].x;
			ty = predecesseur[curX][curY].y;
			curX = tx; 
			curY = ty;
		}
	}
}

///////////////////////////////////////////////////////
///            Fonctions pour Canvas             /////
/////////////////////////////////////////////////////

function renduCanvas() {

	ctx.beginPath(); // commence un nouveau chemin en vidant la liste des sous-chemins

	// on efface le fond
	ctx.fillStyle = "#000000"; 
	ctx.fillRect(0, 0, 800, 600);
	ctx.strokeStyle = "#FFFFFF"; 
	ctx.font = "16px Arial";

	// on trace le quadrillage
	for (i = 0; i < 40; i++) {

		ctx.moveTo(i * 20, 0); ctx.lineTo(i * 20, 600);

		if (i <= 29) {
			ctx.moveTo(0, i * 20); ctx.lineTo(800, i * 20);
		}
	}

	// appel de l'algo de Dijkstra 
	Dijkstra();

	// affichage des éléments de la grille
	for (i = 0; i < 40; i++) {

		for (j = 0; j < 30; j++) {

			// le départ
			if (map[i][j] == 1) {
				ctx.fillStyle = "#0000FF"; ctx.fillRect(i * 20 + 1, j * 20 + 1, 18, 18);
				ctx.fillStyle = "#FFFFFF"; ctx.fillText("D", i * 20 + 5, j * 20 + 15);
			}

			// l'arrivée
			if (map[i][j] == 2) {
				ctx.fillStyle = "#FF0000"; ctx.fillRect(i * 20 + 1, j * 20 + 1, 18, 18);
				ctx.fillStyle = "#FFFFFF"; ctx.fillText("F", i * 20 + 5, j * 20 + 15);
			}

			// les murs
			if (map[i][j] == 9) {
				ctx.fillStyle = "#FFFFFF"; ctx.fillRect(i * 20 + 1, j * 20 + 1, 18, 18);
			}

			// le tracé du chemin final
			if (map[i][j] == 8) {
				ctx.fillStyle = "#FFFF00"; ctx.fillRect(i * 20 + 1, j * 20 + 1, 18, 18);
			}

			// les zones explorées
			if (!Q[i][j] && !(i == 1 && j == posDepart) && !(i == 48 && j == posFin)) {
				ctx.fillStyle = "#FF0000"; ctx.fillRect(i * 20 + 7, j * 20 + 7, 5, 5);
			}
		}
	}

	ctx.stroke();

	// réinitialisation de l'algorithme ?
	if (keyPresses.i) {
		Dijkstra_init();
	}

	// reset complet et génération d'une nouvelle map
	if (keyPresses.n) {
		init();
	}

	setTimeout(function () { // on rafraichi la page "fps" fois par seconde
		requestAnimationFrame(renduCanvas);
	}, 1000 / fps)
}

// lancement
init();

// début de la boucle de rendu
requestAnimationFrame(renduCanvas);