
# Algorithmes

- Dijkstra

# Utlisation

L'application va afficher au tour par tour, les différentes étapes de Algorithme dans un canvas.

À la fin de la recherche (position arriver, trouvé), le chemin le plus court est afficher en Jaune.

Un compteur ("Nbr de déplacement") est mis enplace. Chaque fois que l'algorithme avance dans sa recherche, 
il actualise le compteur avec le nombre de deplacement minimum à éfféctuer.

## Propriétés

- Canvas: 40 cases / 30 cases

- Aucune direction en diagonal (peut être modifier dans la fonction "Dijkstra_poids", ligne 198).

- Génération des obstacles aléatoire.

- Création systématique d'une map pouvant toujours trouver un chemin (cas "aucun chemin", impossible).

## Touches 

- Touche I: réinitialiser déroulement
- Touche N: générer une nouvelle carte

## Légende

- case noire: chemin libre
- case blanche: mur infranchissable
- case bleu: départ
- case rouge: arrivée
- point rouge: chemin parcourue





